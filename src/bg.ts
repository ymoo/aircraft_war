/**
 * 循环滚动的游戏背景
 */
class BackGround extends Laya.Sprite {
    //定义背景1
    private bg1:Laya.Sprite;
    //定义背景2
    private bg2:Laya.Sprite;
    constructor() {
        super();
        this.init();
    }
    private init():void{
        this.bg1 = new Laya.Sprite()
        this.bg2 = new Laya.Sprite()
        this.bg1.loadImage('res/war/background.png')
        this.bg2.loadImage('res/war/background.png')
        this.bg2.pos(0,-852)
        this.addChild(this.bg1)
        this.addChild(this.bg2)
        Laya.timer.frameLoop(1,this,this.onLoop)
    }
    private onLoop():void{
        this.y += 1;
        if(this.bg1.y+this.y>=852){
            this.bg1.y-=852*2;
        }
        if(this.bg2.y+this.y>=852){
            this.bg2.y-=852*2;
        }
    }
}