
import View=laya.ui.View;
import Dialog=laya.ui.Dialog;
module ui {
    export class GameInfoUI extends View {
		public pauseBut:Laya.Button;
		public hpLabel:Laya.Label;
		public levelLabel:Laya.Label;
		public scoreLabel:Laya.Label;
		public infoLabel:Laya.Label;

        public static  uiView:any ={"type":"View","props":{"width":400,"height":852},"child":[{"type":"Button","props":{"y":12,"x":329,"var":"pauseBut","stateNum":1,"skin":"war/btn_pause.png"}},{"type":"Label","props":{"y":14,"x":13,"width":74,"var":"hpLabel","valign":"bottom","text":"HP:0","name":"item0","height":30,"fontSize":20,"color":"#0b9e23","bold":true}},{"type":"Label","props":{"y":14,"x":95,"width":74,"var":"levelLabel","valign":"bottom","text":"level:0","name":"item1","height":30,"fontSize":20,"color":"#0d8b81","bold":true}},{"type":"Label","props":{"y":14,"x":202,"width":132,"var":"scoreLabel","valign":"bottom","text":"score:0","name":"item2","height":30,"fontSize":20,"color":"#869e0b","bold":true}},{"type":"Label","props":{"y":334,"x":72,"wordWrap":true,"width":261,"var":"infoLabel","height":102,"fontSize":25,"color":"#0c6440","align":"center"}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.GameInfoUI.uiView);

        }

    }
}
