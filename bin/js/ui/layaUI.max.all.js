var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var ui;
(function (ui) {
    var GameInfoUI = /** @class */ (function (_super) {
        __extends(GameInfoUI, _super);
        function GameInfoUI() {
            return _super.call(this) || this;
        }
        GameInfoUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.GameInfoUI.uiView);
        };
        GameInfoUI.uiView = { "type": "View", "props": { "width": 400, "height": 852 }, "child": [{ "type": "Button", "props": { "y": 12, "x": 329, "var": "pauseBut", "stateNum": 1, "skin": "war/btn_pause.png" } }, { "type": "Label", "props": { "y": 14, "x": 13, "width": 74, "var": "hpLabel", "valign": "bottom", "text": "HP:0", "name": "item0", "height": 30, "fontSize": 20, "color": "#0b9e23", "bold": true } }, { "type": "Label", "props": { "y": 14, "x": 95, "width": 74, "var": "levelLabel", "valign": "bottom", "text": "level:0", "name": "item1", "height": 30, "fontSize": 20, "color": "#0d8b81", "bold": true } }, { "type": "Label", "props": { "y": 14, "x": 202, "width": 132, "var": "scoreLabel", "valign": "bottom", "text": "score:0", "name": "item2", "height": 30, "fontSize": 20, "color": "#869e0b", "bold": true } }, { "type": "Label", "props": { "y": 334, "x": 72, "wordWrap": true, "width": 261, "var": "infoLabel", "height": 102, "fontSize": 25, "color": "#0c6440", "align": "center" } }] };
        return GameInfoUI;
    }(View));
    ui.GameInfoUI = GameInfoUI;
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map