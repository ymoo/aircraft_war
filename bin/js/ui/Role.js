var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Role = /** @class */ (function (_super) {
    __extends(Role, _super);
    function Role() {
        var _this = _super.call(this) || this;
        //射击类型
        _this.shootType = 0;
        //射击间隔
        _this.shootInterval = 500;
        //下次射击时间
        _this.shootTime = Laya.Browser.now() + 2000;
        //当前动作
        _this.action = "";
        //是否是子弹
        _this.isBullet = false;
        //0：普通；1：子弹；2：炸药；3：补给品
        _this.heroType = 0;
        return _this;
    }
    /**
     *  <p>初始化要显示的角色 </p>
     * @param type 显示的类型（精灵）
     * @param camp 阵营
     * @param hp 血量
     * @param speed 速度
     * @param hitRadius 碰撞半径
     * @param heroType  0：普通；1：子弹；2：炸药；3：补给品
     */
    Role.prototype.init = function (type, camp, hp, speed, hitRadius, heroType) {
        if (heroType === void 0) { heroType = 0; }
        this.type = type;
        this.camp = camp;
        this.hp = hp;
        this.speed = speed;
        this.hitRadius = hitRadius;
        this.heroType = heroType;
        if (!Role.cached) {
            Role.cached = true;
            Laya.Animation.createFrames(['war/hero_fly1.png', 'war/hero_fly2.png'], 'hero_fly');
            Laya.Animation.createFrames(['war/hero_down1.png', 'war/hero_down2.png', 'war/hero_down3.png'], 'hero_down');
            Laya.Animation.createFrames(['war/enemy1_fly1.png'], 'enemy1_fly');
            Laya.Animation.createFrames(['war/enemy2_fly1.png'], 'enemy2_fly');
            Laya.Animation.createFrames(['war/enemy3_fly1.png', 'war/enemy3_fly2.png'], 'enemy3_fly');
            Laya.Animation.createFrames(['war/enemy1_down1.png', 'war/enemy1_down2.png', 'war/enemy1_down3.png'], 'enemy1_down');
            Laya.Animation.createFrames(['war/enemy2_down1.png', 'war/enemy2_down2.png', 'war/enemy2_down3.png'], 'enemy2_down');
            Laya.Animation.createFrames(['war/enemy3_down1.png', 'war/enemy3_down2.png', 'war/enemy3_down3.png',
                'war/enemy3_down4.png', 'war/enemy3_down5.png', 'war/enemy3_down6.png'
            ], 'enemy3_down');
            //缓存敌机2碰撞动作
            Laya.Animation.createFrames(["war/enemy2_hit.png"], "enemy2_hit");
            //缓存敌机3碰撞动作
            Laya.Animation.createFrames(["war/enemy3_hit.png"], "enemy3_hit");
            Laya.Animation.createFrames(["war/bullet1.png"], "bullet1_fly");
            Laya.Animation.createFrames(["war/ufo1.png"], "ufo1_fly");
            Laya.Animation.createFrames(["war/ufo2.png"], "ufo2_fly");
        }
        if (!this.body) {
            //创建一个动画作为飞机的身体
            this.body = new Laya.Animation();
            //把机体添加到容器内
            this.addChild(this.body);
            //添加动画播放完成事件
            this.body.on(Laya.Event.COMPLETE, this, this.onPlayComplete);
        }
        //播放飞机动画
        this.playAction("fly");
    };
    /**
     * 根据类型播放动画
     * fyl| down | hit ,fyl正常飞行，down死亡爆炸动画，hit子弹飞行动画
     * @param action  fyl| down | hit ,fyl正常飞行，down死亡爆炸动画，hit子弹飞行动画
     */
    Role.prototype.playAction = function (action) {
        //记录当前播放动画类型
        this.action = action;
        //根据类型播放动画
        this.body.play(0, true, this.type + '_' + action);
        //获取动画大小区域
        var bound = this.body.getBounds();
        //设置机身居中
        this.body.pos(-bound.width / 2, -bound.height / 2);
    };
    Role.prototype.onPlayComplete = function () {
        //如果是击毁动画，则隐藏对象
        if (this.action === "down") {
            //停止动画播放
            this.body.stop();
            //隐藏显示
            this.visible = false;
        }
        else if (this.action === "hit") {
            //如果是被击动画播放完毕，则接着播放飞行动画
            this.playAction("fly");
        }
    };
    //是否缓存了动画
    Role.cached = false;
    return Role;
}(Laya.Sprite));
//# sourceMappingURL=Role.js.map